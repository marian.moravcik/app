import os

SECRET_KEY = os.getenv('SECRET_KEY', 'secret key')
DEBUG = os.getenv('DEBUG', False)

SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', 'postgresql+psycopg2://app:Password@localhost/app')
