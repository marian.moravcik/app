
# Base Image
FROM python:3.10.2-slim-buster

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

ENV APP_HOME=/web
ENV FLASK_APP="run:app"
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# Install Requirements
COPY ./requirements.txt $APP_HOME
COPY ./ $APP_HOME
RUN pip install --no-cache-dir -r requirements.txt
RUN rm -rf /tmp

EXPOSE 5000

CMD gunicorn --log-level=info -b 0.0.0.0:5000 -w 1 run:app
