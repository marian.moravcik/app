from datetime import datetime
from dateutil.relativedelta import relativedelta

from flask import render_template
from flask_login import login_required
from sqlalchemy import cast, Date

from . import home
from .. import db
from ..models import Orders, Customers



@home.route('/')
@login_required
def homepage():
    earnings = db.session.query(db.func.sum(Orders.total_price))
    earning_year = earnings.filter(cast(Orders.created_at, Date) >= datetime.now() - relativedelta(years=1)).scalar()
    earning_month = earnings.filter(cast(Orders.created_at, Date) >= datetime.now() - relativedelta(months=1)).scalar()
    customers = db.session.query(Customers)\
        .filter(Customers.orders != None).limit(5).all()

    # Sort customers by Total Price from high to low price
    b = sorted([
        dict(labels=c.full_name, data=sum(p.total_price for p in c.orders))
        for c in customers
    ], key=lambda d: d['data'], reverse=True)

    # Redesign Dict for chart
    if customers:
        best_customers_graph = {k: [dic[k] for dic in b] for k in b[0]}
    else:
        best_customers_graph = dict(labels=[], data=[])

    return render_template('home/index.html',
                           title="Dashboard",
                           earning_year=earning_year or 0,
                           earning_month=earning_month or 0,
                           best_customers_graph=best_customers_graph)
