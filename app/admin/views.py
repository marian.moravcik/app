from flask import flash, redirect, render_template, url_for
from flask_login import login_required

from . import admin
from .. import db
from .forms import CustomerForm, OrdersForm, ItemsForm
from ..models import Customers, Orders, Items


@admin.get('/customers/')
@login_required
def list_customers():
    """
    List all customers
    """

    customers = Customers.query.order_by(Customers.id.desc()).all()

    return render_template('admin/customers/customers.html',
                           customers=customers, title="Customers")


@admin.get('/customers/details/<int:id>')
@login_required
def list_customer_details(id):
    """
    List customer details
    """

    customer = Customers.query.get_or_404(id)

    return render_template('admin/customers/details.html',
                           customer=customer, title=f"Details for {customer.full_name}")


@admin.route('/customers/add', methods=['GET', 'POST'])
@login_required
def add_customer():
    """
    Add a customer to the database
    """

    form = CustomerForm()
    if form.validate_on_submit():
        customer = Customers(first_name=form.first_name.data,
                             last_name=form.last_name.data)
        db.session.add(customer)
        db.session.commit()
        flash('You have successfully added a new customer.')

        # redirect to customers page
        return redirect(url_for('admin.list_customers'))

    # load customer template
    return render_template('admin/customers/customer.html', action="Add",
                           form=form,
                           title="Add Customer")


@admin.route('/customers/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_customer(id):
    """
    Edit a customer
    """

    customer = Customers.query.get_or_404(id)
    form = CustomerForm(obj=customer)
    if form.validate_on_submit():
        customer.first_name = form.first_name.data
        customer.last_name = form.last_name.data
        db.session.commit()
        flash('You have successfully edited the customer.')

        # redirect to the customers page
        return redirect(url_for('admin.list_customers'))

    form.first_name.data = customer.first_name
    form.last_name.data = customer.last_name
    return render_template('admin/customers/customer.html', action="Edit",
                           form=form,
                           customer=customer, title="Edit Customer")


@admin.route('/customers/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_customer(id):
    """
    Delete a customer from the database
    """

    customer = Customers.query.get_or_404(id)
    db.session.delete(customer)
    db.session.commit()
    flash('You have successfully deleted the customer.')

    # redirect to the customers page
    return redirect(url_for('admin.list_customers'))


@admin.get('/orders/')
@login_required
def list_orders():
    """
    List all orders
    """
    orders = Orders.query.order_by(Orders.order_number.desc()).all()
    return render_template('admin/orders/orders.html',
                           orders=orders, title='Orders')


@admin.route('/orders/add/', methods=['GET', 'POST'])
@login_required
def add_order():
    """
    Add order to the database
    """

    form = OrdersForm()
    if form.validate_on_submit():
        order = Orders(customer=form.customer.data,
                       description=form.description.data,
                       items=form.items.data)

        # add role to the database
        db.session.add(order)
        db.session.commit()
        flash('You have successfully added a new order.')

        # redirect to the roles page
        return redirect(url_for('admin.list_orders'))

    # load role template
    return render_template('admin/orders/order.html',
                           form=form, title='Add Order')


@admin.route('/orders/edit/<int:order_number>', methods=['GET', 'POST'])
@login_required
def edit_order(order_number):
    """
    Edit order
    """

    order = Orders.query.get_or_404(order_number)
    form = OrdersForm(obj=order)
    if form.validate_on_submit():
        order.customer = form.customer.data
        order.description = form.description.data
        order.items = form.items.data
        db.session.add(order)
        db.session.commit()
        flash('You have successfully edited the order.')

        # redirect to the roles page
        return redirect(url_for('admin.list_orders'))

    form.description.data = order.description
    form.customer.data = order.customer
    form.items.data = order.items
    return render_template('admin/orders/order.html',
                           form=form, title="Edit Order")


@admin.route('/orders/delete/<int:order_number>', methods=['GET', 'POST'])
@login_required
def delete_order(order_number):
    """
    Delete order from the database
    """

    role = Orders.query.get_or_404(order_number)
    db.session.delete(role)
    db.session.commit()
    flash('You have successfully deleted the role.')

    # redirect to the roles page
    return redirect(url_for('admin.list_orders'))


@admin.get('/items/')
@login_required
def list_items():
    """
    List all orders
    """
    items = Items.query.order_by(Items.id.desc()).all()
    return render_template('admin/items/items.html',
                           items=items, title='Items')


@admin.route('/items/add/', methods=['GET', 'POST'])
@login_required
def add_item():
    """
    Add item to the database
    """

    form = ItemsForm()
    if form.validate_on_submit():
        item = Items(name=form.name.data,
                     description=form.description.data,
                     price=form.price.data)

        # add role to the database
        db.session.add(item)
        db.session.commit()
        flash('You have successfully added a new item.')

        # redirect to the roles page
        return redirect(url_for('admin.list_items'))

    # load role template
    return render_template('admin/items/item.html',
                           form=form, title='Add Item')


@admin.route('/items/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_item(id):
    """
    Edit item
    """

    item = Items.query.get_or_404(id)
    form = ItemsForm(obj=item)
    if form.validate_on_submit():
        item.name = form.name.data
        item.price = form.price.data
        item.description = form.description.data
        db.session.add(item)
        db.session.commit()
        flash('You have successfully edited the item.')

        # redirect to the roles page
        return redirect(url_for('admin.list_items'))

    form.description.data = item.description
    form.name.data = item.name
    form.price.data = item.price
    return render_template('admin/items/item.html',
                           form=form, title="Edit Item")


@admin.route('/items/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_item(id):
    """
    Delete order from the database
    """

    item = Items.query.get_or_404(id)
    db.session.delete(item)
    db.session.commit()
    flash('You have successfully deleted the item.')

    # redirect to the roles page
    return redirect(url_for('admin.list_items'))
