from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, FloatField
from wtforms.validators import DataRequired, Length
from wtforms_sqlalchemy.fields import QuerySelectMultipleField, QuerySelectField

from app import db
from app.models import Customers, Items


class CustomerForm(FlaskForm):
    """
    Form to add or edit a customers
    """
    first_name = StringField('First Name', validators=[DataRequired(), Length(max=60)])
    last_name = StringField('Last Name', validators=[DataRequired(), Length(max=60)])
    submit = SubmitField('Submit')


class OrdersForm(FlaskForm):
    """
    Form to add or edit a orders
    """
    description = TextAreaField('Description', validators=[DataRequired(), Length(max=200)])
    customer = QuerySelectField('Customer',
                                query_factory=lambda: db.session.query(Customers),
                                get_label=lambda c: f'{c.first_name} {c.last_name}',
                                validators=[DataRequired()])
    items = QuerySelectMultipleField('Items to order',
                                     query_factory=lambda: db.session.query(Items),
                                     get_label=lambda i: i.name,
                                     validators=[DataRequired()])
    submit = SubmitField('Submit')


class ItemsForm(FlaskForm):
    """
    Form to add or edit a items
    """
    name = StringField('Name', validators=[DataRequired(), Length(max=60)])
    description = TextAreaField('Description', validators=[DataRequired(), Length(max=200)])
    price = FloatField('Price', validators=[DataRequired()])
    submit = SubmitField('Submit')
