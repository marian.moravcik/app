from flask_login import UserMixin
from sqlalchemy import func, and_, Numeric
from sqlalchemy.ext.hybrid import hybrid_property
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime

from app import db, login_manager


class Users(UserMixin, db.Model):
    """
    Employee table model
    """

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(60), unique=True, nullable=False)
    first_name = db.Column(db.String(60), nullable=False)
    last_name = db.Column(db.String(60), nullable=False)
    password_hash = db.Column(db.String(128))

    @property
    def password(self):
        """
        Prevent password from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return f'<User: {self.username}>'


# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))


class Customers(db.Model):
    """
    Customers table model
    """

    __tablename__ = 'customers'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(60))
    last_name = db.Column(db.String(60))
    orders = db.relationship('Orders', backref='customer')

    @hybrid_property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __repr__(self):
        return f'<Customer: {self.first_name} {self.last_name}>'


item_order = db.Table(
    "item_order", db.metadata,
    db.Column("item_id", db.Integer, db.ForeignKey("items.id"), primary_key=True),
    db.Column("order_id", db.Integer, db.ForeignKey("orders.order_number"), primary_key=True)
)


class Items(db.Model):
    """
    Items table model
    """

    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    price = db.Column(db.Float, nullable=False)
    description = db.Column(db.String())
    orders = db.relationship('Orders', secondary='item_order', back_populates="items")

    def __repr__(self):
        return f'<Item: {self.name}>'


class Orders(db.Model):
    """
    Orders table model
    """

    __tablename__ = 'orders'

    order_number = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(200))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'), nullable=False)
    items = db.relationship('Items', secondary='item_order', back_populates='orders')
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    total_price = db.column_property(
        db.select([func.cast(func.sum(Items.price), Numeric(10, 2))],
                  and_(item_order.c.order_id == order_number,
                       item_order.c.item_id == Items.id),
                  item_order).scalar_subquery())
